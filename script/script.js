"use strict";

//  ****************** Our services   *************************

/*   За допомогою jQuery + анімація */
$(document).ready(function () {
  $(".services-item-btn").click(function (e) {
    $(".services-item-btn").removeClass("btn-active");
    $(".services-item-content-wrap").hide();
    $(e.target.getAttribute("data-service")).fadeIn(1000);
    $(e.target).addClass("btn-active");
  });
});

/*   чистий JS 
const tabs = document.querySelectorAll(".services-item-btn");
const tabText = document.querySelectorAll(".services-item-content-wrap");
tabs.forEach((el) => {
  el.addEventListener("click", function (event) {
    tabs.forEach((el) => {
      el.classList.remove("btn-active");
    });
    const liId = event.target.getAttribute("data-service");
    let activeLi = document.querySelector(liId);
    tabText.forEach((el) => {
      el.classList.remove("active-service");
    });
    activeLi.classList.add("active-service");
    el.classList.add("btn-active");
  });
});

*/

//  *************************** Galery   ***************************************

const galery = document.querySelector(".galery-list");
// const hiddenImg = document.querySelectorAll(".galery-loadmore");
const galeryBtn = document.querySelector(".galery-load");

let imgBranch = 1;
function loadFromServer() {
  if (document.querySelectorAll(".galery-item").length === 12) {
    addImage("LoadFirst");
  } else if (document.querySelectorAll(".galery-item").length === 24) {
    addImage("LoadSecond");
  }

  let category = document.querySelector(".active").getAttribute("data-branch");
  if (category) {
    let activeImg = document.querySelectorAll(".galery-list > li");
    activeImg.forEach((el) => {
      el.classList.add("galery-hiden");
    });
    let clickedCat = document.querySelectorAll(category);
    clickedCat.forEach((el) => {
      let img = el.closest("li");
      img.classList.remove("galery-hiden");
    });
  }
}

function addImage(foulder) {
  for (let i = 1; i <= 12; i++) {
    let department = "";
    switch (imgBranch) {
      case 1:
        department = "wordpress";
        break;
      case 2:
        department = "landing-page";
        break;
      case 3:
        department = "web-Design";
        break;
      case 4:
        department = "graphic-Design";
        break;
    }

    if (imgBranch === 4) {
      imgBranch = 1;
    } else {
      imgBranch++;
    }
    let parToShow;
    if (department.split("-")[1]) {
      parToShow = department.split("-")[0] + " " + department.split("-")[1];
    } else {
      parToShow = department;
    }
    let loadLi = document.createElement("li");
    loadLi.classList.add("galery-item");

    loadLi.innerHTML = ` <img
        src="./img/${foulder}/${i}-image.jpg"
        width="286"
        height="211"
        alt= ${department}
        id = ${department}
      />
      <div class="galery-item-details">
        <div class="galery-item-details-btn">
          <button class="galery-details-btn"></button>
          <button class="galery-details-btn"></button>
        </div>
        <h4 class="galery-item-title">creative design</h4>
        <p class="galery-item-branch">${parToShow}</p>
      </div>`;
    galery.append(loadLi);
  }
}

const workMenu = document.querySelector(".work-menu");
workMenu.addEventListener("click", showCat);

function showCat(e) {
  document
    .querySelectorAll(".work-btn")
    .forEach((el) => el.classList.remove("active"));
  e.target.classList.add("active");

  if (e.target.tagName === "LI") {
    if (e.target.classList.contains("btn_all")) {
      let activeImg = document.querySelectorAll(".galery-list > li");

      activeImg.forEach((el) => {
        el.classList.remove("galery-hiden");
      });
    } else {
      let activeImg = document.querySelectorAll(".galery-list > li");
      activeImg.forEach((el) => {
        el.classList.add("galery-hiden");
      });
      let clickedCat = document.querySelectorAll(
        e.target.getAttribute("data-branch")
      );
      clickedCat.forEach((el) => {
        let img = el.closest("li");
        img.classList.remove("galery-hiden");
      });
    }
  }
}

// *****************  SLIDER   *****************

let counter = 1;
let id;

$(document).ready(function () {
  let counter = 1;
  let id;
  $(".slider-left").click(function (e) {
    if (+counter === 1) {
      counter = 4;
    } else {
      counter--;
    }
    $(".feedback-content-item").hide();
    $(`#block-${counter}`).fadeIn(1000);

    $(".slider-photo").removeClass("slider-active");
    $(".slider-photo").css("transform", "translateY(0)");
    $(`#slide-${counter}`).css("transform", "translateY(-30px)");
    $(`#slide-${counter}`).css("transition-duration", "1s");
  });

  $(".slider-right").click(function (e) {
    if (+counter === 4) {
      counter = 1;
    } else {
      counter++;
    }
    $(".feedback-content-item").hide();
    $(`#block-${counter}`).fadeIn(1000);

    $(".slider-photo").removeClass("slider-active");
    $(".slider-photo").css("transform", "translateY(0)");

    $(`#slide-${counter}`).css("transform", "translateY(-30px)");
    $(`#slide-${counter}`).css("transition-duration", "1s");
  });

  $(".slider-photo").click(function (e) {
    $(".slider-photo").removeClass("slider-active");
    $(".slider-photo").css("transform", "translateY(0)");
    let target = e.target.id;
    $(`#${target}`).css("transform", "translateY(-30px)");
    $(`#${target}`).css("transition-duration", "1s");
    $(".feedback-content-item").hide();
    counter = e.target.id.split("-")[1];
    $(`#block-${counter}`).fadeIn(700);
  });
});

/* Slider чистий JS
// const prevBtn = document.querySelector(".slider-left");
// const nextBtn = document.querySelector(".slider-right");

// let counter = 1;
// let id;
// prevBtn.addEventListener("click", function (e) {
//   if (+counter === 1) {
//     counter = 4;
//   } else {
//     counter--;
//   }
//   document
//     .querySelector(".feedback-active")
//     .classList.remove("feedback-active");
//   document.querySelector(".slider-active").classList.remove("slider-active");

//   let blockToShow = document.querySelector(`#block-${counter}`);
//   blockToShow.classList.add("feedback-active");
//   let slideImgUp = document.querySelector(`#slide-${counter}`);
//   slideImgUp.classList.add("slider-active");
// });

// nextBtn.addEventListener("click", function (e) {
//   if (+counter === 4) {
//     counter = 1;
//   } else {
//     counter++;
//   }
//   document
//     .querySelector(".feedback-active")
//     .classList.remove("feedback-active");
//   document.querySelector(".slider-active").classList.remove("slider-active");

//   let blockToShow = document.querySelector(`#block-${counter}`);
//   blockToShow.classList.add("feedback-active");
//   let slideImgUp = document.querySelector(`#slide-${counter}`);
//   slideImgUp.classList.add("slider-active");
// });

// const sliderBlock = document.querySelector(".slider");
// sliderBlock.addEventListener("click", function (e) {
//   if (e.target.tagName === "IMG") {
//     document.querySelector(".slider-active").classList.remove("slider-active");
//     id = e.target.id;
//     let slideImgUp = document.querySelector(`#${id}`);
//     slideImgUp.classList.add("slider-active");
//     counter = e.target.id.split("-")[1];

//     document
//       .querySelector(".feedback-active")
//       .classList.remove("feedback-active");
//     let blockToShow = document.querySelector(`#block-${counter}`);
//     blockToShow.classList.add("feedback-active");
//   }
// });

/* Masonry */

/*  **********************   MASONRY ************************* */

let $container = $(".masonry-galery");
$container.imagesLoaded(function () {
  $container.masonry({
    itemSelector: ".grid",
    columnWidth: ".grid",
    gutter: 20,
  });

  let msnryInner = $(".grid-wrap").masonry({
    itemSelector: ".grid-inner",
    gutter: 3,
  });

  let msnryInnerLittle = $(".grid-inner-little").masonry({
    itemSelector: ".inner-2",
    gutter: 3,
  });
});

$(document).ready(masonryHover);

function masonryHover() {
  $(".grid").hover(
    function (e) {
      $(this).find(".cover-item-galery").css("display", "flex");
    },
    function () {
      $(this).find(".cover-item-galery").fadeOut();
    }
  );
  $(".grid-inner-big").hover(
    function (e) {
      {
        $(this).find(".cover-item-galery").css("display", "flex");
      }
      e.stopPropagation();
    },
    function () {
      $(this).find(".cover-item-galery").fadeOut();
    }
  );

  $(".inner-2").hover(
    function (e) {
      $(this).find(".cover-item-galery").css("display", "flex");
      e.stopPropagation();
    },
    function () {
      $(this).find(".cover-item-galery").fadeOut();
    }
  );
}

function addImageMasonry() {
  for (let i = 1; i <= 7; i++) {
    let loadDiv = document.createElement("div");
    loadDiv.classList.add("grid");
    $(loadDiv).html(`<img src="./img/Masonry/img-${i}.jpg" alt="landscape" />
<div class="cover-item-galery">
  <button class="svg" type="button">
    <i class="fa fa-search" aria-hidden="true"></i>
  </button>
  <button class="svg" type="button">
    <img src="./img/icons8-расширить-50.png" alt="" />
  </button>
</div>`);
    $(".masonry-galery").append(loadDiv);
  }

  let container = document.querySelector(".masonry-galery");
  let msnry;
  // ініціалізація Масонрі, після загрузки картинок
  imagesLoaded(container, function () {
    msnry = new Masonry(container, {
      itemSelector: ".grid",
      columnWidth: ".grid",
      gutter: 20,
    });
    masonryHover();
  });
}

// додаємо анімацію на кнопку загрузки з сервера і окремими функціями підгружаємо нові картинки

function addLoadAnimation(ev) {
  if (ev.target.id === "work-galery") {
    loadFromServer();
  } else addImageMasonry();
  let target = ev.target;

  $(target).attr("disabled", "");
  $(target).html(`<div class="preloader">
  <svg class="preloader__image" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
    <path fill="currentColor"
      d="M304 48c0 26.51-21.49 48-48 48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48 48zm-48 368c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zm208-208c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zM96 256c0-26.51-21.49-48-48-48S0 229.49 0 256s21.49 48 48 48 48-21.49 48-48zm12.922 99.078c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48c0-26.509-21.491-48-48-48zm294.156 0c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48c0-26.509-21.49-48-48-48zM108.922 60.922c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.491-48-48-48z">
    </path>
  </svg>
</div>`);
  let galleryTimerId = setTimeout(() => {
    $(target).html("Load More");
    $(target).removeAttr("disabled");
    if (
      document.querySelectorAll(".galery-item").length === 36 ||
      target.id === "masonry-load"
    ) {
      target.remove();
    }
  }, 2000);
}

/* вішаємо один обробник подій на кнопку загрузки фото з сервера */
$(".galery-load").click(function (e) {
  addLoadAnimation(e);
});
